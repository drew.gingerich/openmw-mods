# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild import DOWNLOAD_DIR, Pybuild2


class Package(Pybuild2):
    NAME = "7-zip"
    DESC = "A file archiver with a high compression ratio"
    HOMEPAGE = "https://www.7-zip.org/"
    SRC_URI = """
        platform_linux? ( https://www.7-zip.org/a/7z2201-linux-x64.tar.xz )
        platform_darwin? ( https://www.7-zip.org/a/7z2107-mac.tar.xz )
        platform_win32? (
            https://www.7-zip.org/a/7z2201-x64.exe
            http://www.7-zip.org/a/7za920.zip
        )
    """
    LICENSE = "LGPL-2.1+ BSD unRAR"
    KEYWORDS = "openmw"
    IUSE = "platform_linux platform_darwin platform_win32"
    REQUIRED_USE = "^^ ( platform_linux platform_darwin platform_win32 )"

    def src_unpack(self):
        # We need to use 7za from the zip file to extract the exe,
        # which is a self-extracting 7z archive
        if "platform_win32" in self.USE:
            self.unpack(os.path.join(DOWNLOAD_DIR, "7za920.zip"))
            os.chdir("7za920")
            self.execute(
                [
                    "7za.exe",
                    "x",
                    "-o" + os.path.join(self.WORKDIR, "7z2201-x64"),
                    os.path.join(DOWNLOAD_DIR, "7z2201-x64.exe"),
                ]
            )
        else:
            super().src_unpack()

    def src_prepare(self):
        if "platform_linux" in self.USE:
            os.symlink("7zzs", "7z")
        if "platform_darwin" in self.USE:
            os.symlink("7zz", "7z")

    def src_install(self):
        bindest = os.path.join(self.D, "bin")
        os.makedirs(bindest, exist_ok=True)
        if "platform_win32" in self.USE:
            # There are also txt files in a Lang subdirectory which may be needed for localisation
            # But they probably shouldn't be stored in a shared bin directory
            shutil.move("7z.exe", bindest)
            shutil.move("7z.dll", bindest)
        elif "platform_darwin" in self.USE:
            shutil.move("7zz", bindest)
            shutil.move("7z", bindest)
        else:
            shutil.move("7zzs", bindest)
            shutil.move("7z", bindest)
        self.dodoc("*.txt")

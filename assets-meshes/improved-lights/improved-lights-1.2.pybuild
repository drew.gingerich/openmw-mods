# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Improved Lights for All Shaders"
    DESC = "Adjusts every vanilla light mesh to enhance the effects of other shader and lighting mods"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/51463"
    LICENSE = "mw-nc-sa"
    RDEPEND = """
        base/morrowind
        !!items-misc/glowing-flames
    """
    KEYWORDS = "openmw"
    IUSE = "smoke-steam-no-glow"
    DATA_OVERRIDES = "assets-misc/morrowind-optimization-patch"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/51463?tab=files&file_id=1000032220
        -> ILFAS-51463-1-2-1658062476.zip
    """
    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir("01 Smoke and Steam Emitters", REQUIRED_USE="smoke-steam-no-glow"),
    ]
